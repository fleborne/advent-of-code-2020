import sys

def readInputs(fn=None):
    return open(fn if fn is not None else sys.argv[1]).read().strip().split('\n')

get_bin = lambda x, n: format(x, 'b').zfill(n)

def applyMask(mask, value):
    binary = get_bin(int(value), 36)
    l = ''.join([c if c in ['0', '1'] else binary[idx] for idx, c in enumerate(mask)])
    return sum([int(c)*2**idx for idx, c in enumerate(reversed(l))])

def writeMemory(addr, value, mask):
    memory[addr] = int(applyMask(mask, value))

mask = 0
memory = dict()
inputs = readInputs()

for line in inputs:
    cmd, value = line.split(' = ')
    
    if cmd == "mask":
        mask = value
    else:
        addr = int(cmd.replace('mem[', '').replace(']', ''))
        writeMemory(addr, value, mask)

print(sum([memory[key] for key in memory.keys()]))

######################################################################

def applyMask(mask, addr):
    binary = get_bin(int(addr), 36)
    
    new_addr = []
    for idx, c in enumerate(mask):
        if c == '1':
            new_addr.append('1')
        elif c == '0':
            new_addr.append(binary[idx])
        else:
            new_addr.append('X')
    return ''.join(new_addr)

def writeMemory(addr, value, mask):
    addr = applyMask(mask, addr)
    indexes = [i for i, c in enumerate(addr) if c == 'X'] 
    n = 2**len(indexes)
    addrs = []

    for i in range(n):
        b = get_bin(int(i), len(indexes))
        tmp_addr = [c for c in addr]
        for idx, j in enumerate(indexes):
            tmp_addr[j] = b[idx]

        addrs.append(sum([int(c)*2**idx for idx, c in enumerate(reversed(tmp_addr))]))
        memory[addrs[-1]] = value

mask = 0
memory = dict()
inputs = readInputs()

for line in inputs:
    cmd, value = line.split(' = ')
    
    if cmd == "mask":
        mask = value
    else:
        addr = cmd.replace('mem[', '').replace(']', '')
        writeMemory(addr, value, mask)

print(sum([int(memory[key]) for key in memory.keys()]))


