import sys
from functools import reduce

def readInput(filename):
    return open(filename, "r").read().strip().split("\n")

def processInput(inputs):
    seat_ids = map(seatId, inputs)
    max_seat_id = reduce(max, seat_ids)
    sorted_seat_ids = sorted(seat_ids)
    min_seat_id = reduce(min, seat_ids)

    return seat_ids, max_seat_id, sorted_seat_ids, min_seat_id

def seatId(str_id):
    return int(str_id.replace('F', '0')
                     .replace('B', '1')
                     .replace('L', '0')
                     .replace('R', '1'), base=2)

def seatIdStr(int_id):
    str_id = "{0:b}".format(int_id)
    return (str_id[:-3].replace('0', 'F').replace('1', 'B')) \
           + (str_id[-3:].replace('0', 'L').replace('1', 'R'))

def checkIds(inlist, min_seat_id):
    l = len(inlist)
    if l == 0:
        return min_seat_id

    boundary = l//2
    if inlist[boundary] == boundary + min_seat_id:
        # all the ids at lower indexes have the right value
        if l == 1:
            return inlist[0] + 1
        else:
            return checkIds(inlist[boundary+1:], inlist[boundary] + 1)
    else:
        if l == 1:
            return inlist[0] - 1
        else:
            return checkIds(inlist[:boundary], inlist[0])

seat_ids, max_seat_id, sorted_seat_ids, min_seat_id = processInput(readInput(sys.argv[1]))
# seat_ids, max_seat_id, sorted_seat_ids, min_seat_id = processInput(map(seatIdStr, [1, 2, 3, 4, 5, 7]))

print("The answer for the first star is {}".format(max_seat_id))
print("The answer for the second star is {}".format(checkIds(sorted_seat_ids, min_seat_id)))
