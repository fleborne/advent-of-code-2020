inputs = []

with open("inputs.txt", "r") as f:
    for line in f:
        inputs.append(line)

## First star
valid_passwords = 0
for l in inputs:
    rule, password = l.strip().split(": ")
    times, letter = rule.split(' ')
    min, max = times.split('-')

    appearences = 0

    for elt in password:
        if elt == letter:
            appearences += 1

    if int(min) <= appearences and appearences <= int(max):
        valid_passwords += 1

print("The answer is {}".format(valid_passwords))


## Second star
valid_passwords = 0
for l in inputs:
    rule, password = l.strip().split(": ")
    places, letter = rule.split(' ')
    p1, p2 = places.split('-')

    if (password[int(p1)-1] == letter and password[int(p2)-1] != letter) or (password[int(p1)-1] != letter and password[int(p2)-1] == letter):
        valid_passwords += 1

print("The answer is {}".format(valid_passwords))
