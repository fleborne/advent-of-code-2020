import sys
import numpy as np

def readInputs(filename=None):
    return open(filename if filename != None else sys.argv[1], "r").read().strip().split('\n')

def makeLayout(inputs):
    h = len(inputs)
    w = len(inputs[0])
    layout = np.zeros((h, w))

    for i, line in enumerate(inputs):
        for j, spot in enumerate(line):
            if spot == '.':
                layout[i, j] = np.nan
            elif spot == 'L':
                layout[i, j] = 0
            elif spot == '#':
                layout[i, j] = 1
            else:
                raise ValueError("Unexpected symbol {}".format(spot))

    return layout

def getAdjacentSum(layout, i, j):
    return np.nansum(layout[max(0, i-1):min(layout.shape[0], i+2),
                            max(0, j-1):min(layout.shape[1], j+2)])

def getFirstSeatSum(layout, i, j):
    count = 0

    for dir_i in (-1, 0, 1):
        for dir_j in (-1, 0, 1):
            if dir_i == 0 and dir_j == 0: continue
            try:
                distance = 1
                k = i + dir_i
                l = j + dir_j
                if k < 0 or l < 0: raise IndexError()
                while np.isnan(layout[k, l]):
                    distance += 1
                    k = i + dir_i * distance
                    l = j + dir_j * distance
                    if k < 0 or l < 0: raise IndexError()
                count += layout[k, l]
            except IndexError:
                continue
    return count

def updateSeat(layout, i, j, part):
    if np.isnan(layout[i, j]):
        return np.nan
    elif layout[i, j] == 0:
        if (part == 1 and getAdjacentSum(layout, i, j) == 0) or (part == 2 and getFirstSeatSum(layout, i, j) == 0):
            return 1
        else:
            return 0
    elif layout[i, j] == 1:
        if (part == 1 and getAdjacentSum(layout, i, j) > 4) or (part == 2 and getFirstSeatSum(layout, i, j) > 4):
            return 0
        else:
            return 1
    else:
        raise ValueError("Unexpected value {} at coords ({}, {})".format(layout[i,j], i, j))


def countOccupiedSeats(layout):
    return int(np.nansum(layout))

def solve(part):
    layout = makeLayout(readInputs())
    layout_changed = True
    while layout_changed:
        h, w = layout.shape

        new_layout = np.zeros(layout.shape)
        layout_changed = False
        for i in range(h):
            for j in range(w):
                new_layout[i, j] = updateSeat(layout, i, j, part)
                if not np.isnan(layout[i,j]) and new_layout[i, j] != layout[i, j]:
                    layout_changed = True
        layout = np.array(new_layout)

    return countOccupiedSeats(layout)

print(solve(1))
print(solve(2))
