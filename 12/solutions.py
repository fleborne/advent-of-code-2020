import sys
import math

def readInputs(filename=None):
    return open(filename if filename != None else sys.argv[1], "r").read().strip().split('\n')

ship_pos = [0, 0]
ship_dir = [1, 0]

for mov in readInputs():
    dir_ = mov[0]
    dist_ = float(mov[1:])

    if dir_ == 'N':
        ship_pos[1] += dist_
    elif dir_ == 'S':
        ship_pos[1] -= dist_
    elif dir_ == 'E':
        ship_pos[0] += dist_
    elif dir_ == 'W':
        ship_pos[0] -= dist_
    elif dir_ in ['L', 'R']:
        if dir_ == 'R':
            dist_ *= -1.0

        ct = round(math.cos(dist_ / 180.0 * math.pi))
        st = round(math.sin(dist_ / 180.0 * math.pi))

        x = (ct*ship_dir[0] - st*ship_dir[1])
        y = (st*ship_dir[0] + ct*ship_dir[1])
        ship_dir = [x, y]
    elif dir_ == 'F':
        ship_pos[0] += ship_dir[0] * dist_
        ship_pos[1] += ship_dir[1] * dist_
    else:
        raise ValueError(f"Unexpected move {dir_},{dist_}")
    # print(ship_pos)

print(abs(ship_pos[0])+abs(ship_pos[1]))


ship_pos = [0,0]
wp = [10,1]

for mov in readInputs():
    dir_ = mov[0]
    dist_ = float(mov[1:])
    
    if dir_ == 'N':
        wp[1] += dist_
    elif dir_ == 'S':
        wp[1] -= dist_
    elif dir_ == 'E':
        wp[0] += dist_
    elif dir_ == 'W':
        wp[0] -= dist_
    elif dir_ in ['L', 'R']:
        if dir_ == 'R':
            dist_ *= -1.0

        ct = round(math.cos(dist_ / 180.0 * math.pi))
        st = round(math.sin(dist_ / 180.0 * math.pi))

        x = (ct*wp[0] - st*wp[1])
        y = (st*wp[0] + ct*wp[1])
        wp = [x, y]
    elif dir_ == 'F':
        ship_pos[0] += wp[0] * dist_
        ship_pos[1] += wp[1] * dist_
    else:
        raise ValueError(f"Unexpected move {dir_},{dist_}")
    
print(abs(ship_pos[0])+abs(ship_pos[1]))


