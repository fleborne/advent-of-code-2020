import sys

def readInputs(fn=None):
    return open(fn if fn != None else sys.argv[1]).read().strip().split('\n')

inputs = readInputs()
start_timestamp = int(inputs[0])
busses = list(map(int, filter(lambda x: x != 'x', inputs[1].split(','))))

mins_to_wait = list(map(lambda x: x - start_timestamp % x, busses))
best = min(mins_to_wait)
idx = mins_to_wait.index(best)
print(busses[idx] * best)

inputs = readInputs()
busses = inputs[1].split(',')
bus_order = list(map(lambda elt: (elt[0], int(elt[1])), filter(lambda elt: elt[1] != 'x', enumerate(busses))))
bus_order = sorted(bus_order, key=lambda elt: elt[1])
print(bus_order)

def find_(bus_order, start=-1):
    # min_bus_pos, min_bus_id = bus_order[0]
    max_bus_pos, max_bus_id = bus_order[-1]
    found = False
    while not found:
        start += 1
        # tmp = min_bus_id * start
        tmp = max_bus_id * (start+1) - max_bus_pos
        found = True
        
        for bus in bus_order[-2::-1]:
            bus_pos, bus_id = bus
            #print(tmp, bus_pos, bus_id, tmp % bus_id, bus_id-bus_pos)
            if tmp % bus_id == (bus_id - bus_pos) % bus_id:
                continue
            else:
                found = False
                break

    return tmp

def find_sieve(bus_order):
    bus_pos, bus_id = bus_order[-1]
    X = bus_id - bus_pos 
    prod_n = bus_id

    for idx in range(len(bus_order)-2, -1, -1):
        i = 0
        bus_pos, bus_id = bus_order[idx]
        found = False
        while not found:
            tmp = X + i * prod_n
            print(idx, bus_pos, bus_id, X, prod_n, tmp)
            if tmp % bus_id == (bus_id - bus_pos) % bus_id:
                found = True
                X = tmp
                prod_n *= bus_id
                break
            else:
                i += 1
                continue
    return tmp

# print(find_(bus_order, start=int(132800864483603/bus_order[-1][1])))
print(find_sieve(bus_order))


#timestamp = 1
#for elt in bus_order:
#    timestamp *= (elt[1] - elt[0])
#
#print(timestamp)
