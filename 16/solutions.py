import sys

def readInputs():
    return open(sys.argv[1], "r").read().strip().splitlines()

def parseRules(inputs, rules_end):
    rules = []
    for line in inputs[:rules_end]:
        fields = line.split(': ')
        fields = fields[1].split(' or ')
        r1_min, r1_max = fields[0].split('-')
        r2_min, r2_max = fields[1].split('-')

        s1 = set(range(int(r1_min), 1+int(r1_max)))
        s2 = set(range(int(r2_min), 1+int(r2_max)))
        rules.append(set.union(s1, s2))
    return rules 

def isValid(ticket, rules):
    for n in ticket:
        if not n in rules:
            return n
    return -1

def invalidNearbyTickets(inputs, rules, nearby_tickets):
    invalids = []
    valid_tickets = []
    for line in inputs[nearby_tickets:]:
        print("Parsing ticket", line)
        nums = list(map(int, line.split(',')))
        valid = isValid(nums, rules)
        if valid != -1:
            print("  > invalid", valid)
            invalids.append(valid)
        else:
            print("  > valid")
            valid_tickets.append(nums)
    return invalids, valid_tickets

def parseValidTickets(tickets):
    cols = [[] for i in range(len(tickets[0]))]
    for nums in tickets:
        for i, num in enumerate(nums):
            cols[i].append(num)
    return cols

def parseInputs(inputs):
    rules_end = 0
    my_ticket = 0
    nearby_tickets = 0

    for i, line in enumerate(inputs):
        if line == "":
            rules_end = i
            my_ticket = i+2
            nearby_tickets = i+5
            return rules_end, my_ticket, nearby_tickets

inputs = readInputs()
rules_end, my_ticket, nearby_tickets = parseInputs(inputs)
print('rules end', rules_end, 'my ticket', my_ticket, 'nearby', nearby_tickets)

rules = parseRules(inputs, rules_end)
invalids, valids = invalidNearbyTickets(inputs, set.union(*rules), nearby_tickets)
print("First star", sum(invalids))

cols = parseValidTickets(valids)
ref_l = len(cols[0])
assert ref_l == len(valids)
assert len(valids)+len(invalids) == len(inputs)-nearby_tickets
for c in cols:
    assert len(c) == ref_l

field_rule = dict() # key: rule_line; value: num_col
poss_rules = dict() # num_col: rule_line

for i in range(len(cols)):
    poss_rules[i] = []

for idx, col in enumerate(cols):
    for rule in range(rules_end):
        skip = False
        for num in col:
            if num not in rules[rule]:
                skip = True
                break
        if not skip:
            poss_rules[idx].append(rule)

stop = False
while not stop:
    stop = True
    for c in poss_rules.keys():
        if type(poss_rules[c]) == list:
            stop = False

            if len(poss_rules[c]) == 1:
                v = poss_rules[c][0]
                field_rule[v] = c
                poss_rules[c] = v
                for l in poss_rules.values():
                    if type(l) == list:
                        try:
                            l.remove(v)
                        except ValueError:
                            pass
                break

prod = 1
my_nums = list(map(int, inputs[my_ticket].split(',')))

for i in range(6):
    #print(my_nums[field_rule[i]])
    prod *= my_nums[field_rule[i]]

print("Second star", prod)
