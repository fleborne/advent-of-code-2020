import sys

def readInput():
    return open(sys.argv[1], "r").read().strip().splitlines()


def parseLine(line):
    i = 0
    coords = (0, 0)

    while i < len(line):
        if line[i] == 'e':
            coords = (coords[0] + 2, coords[1])
            i += 1
        elif line[i] == 'w':
            coords = (coords[0] - 2, coords[1])
            i += 1
        elif line[i] == 'n':
            if line[i+1] == 'e':
                coords = (coords[0] + 1, coords[1] + 1)
            elif line[i+1] == 'w':
                coords = (coords[0] - 1, coords[1] + 1)
            
            i += 2

        elif line[i] == 's':
            if line[i+1] == 'e':
                coords = (coords[0] + 1, coords[1] - 1)
            elif line[i+1] == 'w':
                coords = (coords[0] - 1, coords[1] - 1)

            i += 2

    return coords

def countBlackNeighbors(coords, black_tiles):
    count = 0
    ccs = [(-1,-1), (-1,1), (1,-1), (1,1), (-2,0), (2,0)]
    for cc in ccs:
        dx, dy = cc
        if (coords[0]+dx, coords[1]+dy) in black_tiles:
            count += 1
    return count
    
def update(black_tiles):
    ccs = [(-1,-1), (-1,1), (1,-1), (1,1), (-2,0), (2,0)]
    new_black_tiles = set()
    updated = []
    
    for bt in black_tiles:
        for cc in ccs:
            dx, dy = cc
            coords = (bt[0]+dx, bt[1]+dy)
            if coords in updated:
                continue
            ngb = countBlackNeighbors(coords, black_tiles)
            if coords in black_tiles:
                if not (ngb == 0 or ngb > 2):
                    new_black_tiles.add(coords)
            elif ngb == 2:
                    new_black_tiles.add(coords)

            updated.append(coords)

    return new_black_tiles


def run():
    inputs = readInput()
    flipped = set()

    for line in inputs:
        coords = parseLine(line)

        if coords in flipped:
            flipped.remove(coords)
        else:
            flipped.add(coords)
    return flipped

def run2():
    black_tiles = run()

    for i in range(100):
        black_tiles = update(black_tiles)
    
    return black_tiles

print(len(run2()))


