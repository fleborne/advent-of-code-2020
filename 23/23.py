def get3Cups(ccupidx, L):
    return tuple((ccupidx+1+i)%L for i in range(3))

def getDestination(ccupidx, cups):
    L = len(cups) 
    dest_label = cups[ccupidx] - 1
    if dest_label < 1:
        dest_label = L

    while dest_label in [cups[i] for i in get3Cups(ccupidx, L)]:
        dest_label -= 1

        if dest_label < 1:
            dest_label = L

    return dest_label

def update(cups, ccupidx):
    L = len(cups)
    new_cups = [0] * L
    ccc = get3Cups(ccupidx, L)
    #print("pick up:", *[cups[i] for i in ccc])
    offset = 0
    dest = getDestination(ccupidx, cups)
    #print("destination", dest)

    cups_i = 0
    new_cups_i = 0
    while new_cups_i < L:
        if cups_i in ccc:
            cups_i = (cups_i + 3)%L
        elif cups[cups_i] == dest:
            new_cups[new_cups_i] = cups[cups_i]
            new_cups[new_cups_i+1:new_cups_i+4] = [cups[i] for i in ccc]
            new_cups_i += 4
            cups_i += 1
            continue
        else:
            new_cups[new_cups_i] = cups[cups_i]
            new_cups_i += 1
            cups_i = (cups_i + 1)%L

    return tuple(new_cups), (new_cups.index(cups[ccupidx])+1)%L

def update2(cups, ccup, nb_cups):
    nexts = [cups[ccup], cups[cups[ccup]], cups[cups[cups[ccup]]]]

    # find destination
    dest = ccup - 1
    if dest < 1: dest = nb_cups
    while dest in nexts:
        dest -= 1
        if dest < 1:
            dest = nb_cups
    
    # place nexts after the destination
    dest_next = cups[dest]
    cups[dest] = nexts[0]

    last_next_next = cups[nexts[-1]]
    cups[nexts[-1]] = dest_next

    # change next of ccup
    cups[ccup] = last_next_next

    return cups, cups[ccup] 

def run():
    cups = (3, 9, 4, 6, 1, 8, 5, 2, 7)
#    cups = (3, 8, 9, 1, 2, 5, 4, 6, 7) # sample data
    ccupidx = 0

    for r in range(100):
        #_ = input(" ")
        #print("------------------------------") 
        #print("cups", cups)
        #print("current", cups[ccupidx])
        cups, ccupidx = update(cups, ccupidx)
    
    start = cups.index(1) + 1
    L = len(cups)
    return ''.join([str(cups[(start+i)%L]) for i in range(L-1)])

def run2(nb_cups, nb_moves):
    init_cups = (3, 9, 4, 6, 1, 8, 5, 2, 7)
    cups = dict() # label: next
    cups[nb_cups] = init_cups[0]
    ccup = init_cups[0]
    
    for i in range(nb_cups - 1):
        if i < 8:
            cups[init_cups[i]] = init_cups[i+1]
        elif i == 8:
            cups[init_cups[i]] = 10
        else:
            cups[i+1] = (i+2)

    for r in range(nb_moves):
        cups, ccup = update2(cups, ccup, nb_cups)
    
    return cups[1] * cups[cups[1]]

print(run())
print(run2(1000000, 10000000))
