import sys

class BagsRules(object):
    def __init__(self):
        self.containers_rules = dict()
        self.containees_rules = dict()

    def addBagContainer(self, containee, container):
        """ Add a potential container to a given bag color """

        _, color = containee

        if color in self.containers_rules.keys():
            self.containers_rules[color].add(container)
        else:
            self.containers_rules[color] = {container}

    def addBagContainees(self, container, containees):
        if container in self.containees_rules:
            raise ValueError(container + " already in rules")
        self.containees_rules[container] = containees

    def addBag(self, container, containees):
        for containee in containees:
            self.addBagContainer(containee, container)


def readInput(filename=None):
    if filename is None: filename = sys.argv[1]
    return open(filename, "r").read().strip().split('\n')

def getParentChildren(line):
    def f(elt):
        try:
            quantity = int(elt[0])
        except ValueError:
            quantity = 0
        return (quantity, elt[2:].split(' bag')[0])

    items = line.strip('.').split(" bags contain ")
    children = items[1].split(", ")
    children = map(f, children)

    return items[0], list(children)

def processInput(inlist):
    bags = BagsRules()
    for line in inlist:
        container, containees = getParentChildren(line)
        bags.addBag(container, containees)
        bags.addBagContainees(container, containees)

    return bags

def findContainers(color, bags_rules):
    if color in bags_rules.keys():
        containers_list = map(lambda elt: findContainers(elt, bags_rules),
                              bags_rules[color])

        return bags_rules[color].union(*containers_list)
    else:
        return set()

def countContainedBags(bag_color, bags_rules):
    def f(elt, bags_rules):
        quantity, color = elt
        return quantity * (1 + countContainedBags(color, bags_rules))

    if not bag_color in bags_rules.keys():
        return 0
    else:
        return sum(map(lambda elt: f(elt, bags_rules), bags_rules[bag_color]))

# Create data structures
bags_rules = processInput(readInput())

# First star
print("First star:", len(findContainers("shiny gold", bags_rules.containers_rules)))

# Second star
print("Second star:", countContainedBags("shiny gold", bags_rules.containees_rules))
