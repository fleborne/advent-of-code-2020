import sys

def readInput(filename=None):
    if filename is None: filename = sys.argv[1]
    return open(filename, "r").read().strip().split('\n')

inputs = list(map(int, readInput()))
preamble_len = 25

# for idx in range(25, len(inputs)):
#     stop = False
#     to_test = inputs[idx]
#
#     for i in range(idx):
#         for j in range(i+1, idx):
#             a = inputs[i]
#             b = inputs[j]
#             if a+b == to_test:
#                 stop = True
#                 break
#         if stop:
#             break
#
#     if not stop:
#         print(to_test)
#         break

to_test = 10884537
stop = False
for i in range(len(inputs)):
    sum_ = inputs[i]
    min_ = inputs[i]
    max_ = inputs[i]
    for j in range(i+1, len(inputs)):
        sum_ += inputs[j]
        min_ = min(min_, inputs[j])
        max_ = max(max_, inputs[j])
        if sum_ == to_test:
            print(min_+max_)
            stop = True
            break
    if stop: break
