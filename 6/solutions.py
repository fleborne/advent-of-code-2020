import sys

def readInput(filename):
    return open(filename, "r").read().strip()

def curateForFirstStar(inputs):
    return inputs.replace('\n\n', '#').replace('\n', '').split("#")

def processGroupForFirstStar(in_str):
    return len(set(in_str))

def curateForSecondStar(inputs):
    return inputs.split('\n\n')

def processGroupForSecondStar(in_str):
    separated_answers = map(set, in_str.split('\n'))
    groups_answers = set.intersection(*separated_answers)

    return len(groups_answers)

print(sum(map(processGroupForFirstStar, curateForFirstStar(readInput(sys.argv[1])))))

print(sum(map(processGroupForSecondStar, curateForSecondStar(readInput(sys.argv[1])))))
