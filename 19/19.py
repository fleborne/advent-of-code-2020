import sys

def readInputs():
    inputs = open(sys.argv[1], "r").read().strip()
    rules, msgs = inputs.split('\n\n')
    return rules.splitlines(), msgs.splitlines()

def parseRules(rules):
    parsed_rules = dict()
    for rule in rules:
        rid, r = rule.split(': ')
        if rid in parsed_rules.keys():
            raise KeyError("Key {} already in dict".format(rid))
        else:
            parsed_rules[rid] = r
    return parsed_rules

def checkListOfMessagesAgainstRule(rule, rules, msg_list, lvl=0):
    b_res, msg_res = [], []
    for msg in msg_list:
        for b0, msg0 in zip(*checkMessageAgainstRule(rule, rules, msg, lvl)):
            if b0:
                b_res.append(b0)
                msg_res.append(msg0)
    return b_res, msg_res

cache = dict()
def checkMessageAgainstRule(rule, rules, msg, lvl=0, debug=False):
    if debug: print("| "*lvl + "msg", msg, "against", rule)

    if rule in cache.keys():
        if msg in cache[rule].keys():
            return cache[rule][msg]
    else:
        cache[rule] = dict()

    if len(msg) < 1:
        cache[rule][msg] = ([False], [''])
        return [False], ['']

    if rule[0] == '"':
        b = msg[0] == rule[1]
        cache[rule][msg] = ([b], [msg[1:]])
        return [b], [msg[1:]]

    elif '|' in rule:
        rs = rule.split(' | ')

        b1, msg1 = checkMessageAgainstRule(rs[0], rules, msg, lvl+1)
        b2, msg2 = checkMessageAgainstRule(rs[1], rules, msg, lvl+1)
        
        b_res, msg_res = [], []
        
        for b_loop, msg_loop in zip(b1, msg1):
            if b_loop:
                b_res.append(b_loop)
                msg_res.append(msg_loop)
        for b_loop, msg_loop in zip(b2, msg2):
            if b_loop:
                b_res.append(b_loop)
                msg_res.append(msg_loop)

        cache[rule][msg] = b_res, msg_res
        return b_res, msg_res

    elif ' ' in rule:
        rs = rule.split(' ')
        b_res, msg_res = [], []
        msg0 = [msg]
        for r in rs:
            b, msg0 = checkListOfMessagesAgainstRule(r, rules, msg0, lvl+1)
        
        for b_loop, msg_loop in zip(b, msg0):
            if b_loop:
                b_res.append(b_loop)
                msg_res.append(msg_loop)

        if len(b_res) > 0:
            cache[rule][msg] = b_res, msg_res
            return b_res, msg_res

        local_cache = [], []
        return [], []

    else:
        return checkMessageAgainstRule(rules[rule], rules, msg, lvl+1)

rules, msgs = readInputs()
parsed_rules = parseRules(rules)

msgs_validity = map(lambda msg: checkMessageAgainstRule('0', parsed_rules, msg), msgs)
msgs_valid_0_count = [1 for elt in msgs_validity if (len(elt[0]) > 0 and '' in elt[1])]

print(sum(msgs_valid_0_count))

