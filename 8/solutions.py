import sys

class Node(object):
    def __init__(self, instr_id, instr_type, arg):
        self.instr_id = instr_id
        self.instr_type = instr_type
        self.arg = arg

        self.toggled = False

def readInput(filename=None):
    if filename is None: filename = sys.argv[1]
    return open(filename, "r").read().strip().split('\n')

def runProgram():
    def findUntoggled(backtrace_level):
        for i in range(backtrace_level, 0, -1):
            if not backtrace_tree[i].toggled:
                backtrace_tree[i].toggled = True
                return i

    def untoggleChildren(id):
        if id+1 < len(backtrace_tree):
            for node in backtrace_tree[id+1:]:
                node.toggled = False

    def backtrace(backtrace_level):
        prog = list(prog_ref)
        id = findUntoggled(backtrace_level)
        untoggleChildren(id)

        new_instr_type = "jmp" if backtrace_tree[id].instr_type == "nop" else "nop"
        prog[backtrace_tree[id].instr_id] = new_instr_type + ' ' + backtrace_tree[id].arg

        for node in backtrace_tree:
            if node.toggled:
                new_instr_type = "jmp" if node.instr_type == "nop" else "nop"
                prog[node.instr_id] = new_instr_type + ' ' + node.arg

        return backtrace_level - 1, prog

    prog_ref = readInput()
    prog = list(prog_ref)

    backtrace_tree = []
    for instr_id, line in enumerate(prog_ref):
        instr_type, arg = line.split(' ')
        backtrace_tree.append(Node(instr_id, instr_type, arg))
    backtrace_level = len(backtrace_tree)-1

    instr_id = 0
    acc_value = 0

    while True:
        if instr_id >= len(prog):
            return acc_value

        instr_type, arg = prog[instr_id].split(' ')
        prog[instr_id] = "stop +0"

        if instr_type == "nop":
            instr_id += 1
        elif instr_type == "acc":
            acc_value += int(arg)
            instr_id += 1
        elif instr_type == "jmp":
            instr_id += int(arg)
        elif instr_type == "stop":
            backtrace_level, prog = backtrace(backtrace_level)
            instr_id = 0
            acc_value = 0
        else:
            raise ValueError("Unexpected instruction '{} {}'".format(instr_type, arg))

print(runProgram())
