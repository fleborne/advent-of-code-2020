import sys
import copy

def readInput():
    input_ = open(sys.argv[1], "r").read().strip().split('\n\n')

    player1 = input_[0].splitlines()[1:]
    player2 = input_[1].splitlines()[1:]

    return list(map(int, player1)), list(map(int, player2))

def computeScore(p):
    return sum([(len(p) - i)*c for i, c in enumerate(p)])

def avoidInfiniteLoop(rounds, p1, p2):
    for r in rounds:
        rp1, rp2 = r
        
        if len(p1) != len(rp1) or len(p2) != len(rp2): 
            continue

        for rc1, c1 in zip(rp1, p1):
            if rc1 != c1: break
        if rc1 != c1: continue
        
        for rc2, c2 in zip(rp2, p2):
            if rc2 != c2: break
        if rc2 != c2: continue
    
        return True
    return False 

subgames = 0
def recursiveCombat(p1, p2, subgame=1):
    global subgames
    subgames += 1
    round_ = 0
    rounds = set()

    m = max(p1)
    if m > max(p2) and m > len(p1) + len(p2):
        return 1, p1

    while True:
        round_ +=1
        #print("-- Round {} (Game {}) --".format(round_, subgame))
        #print("Player 1's deck:", p1)
        #print("Player 2's deck:", p2, '\n')

        tmp = (tuple(p1), tuple(p2))
        if tmp in rounds:
            #print("!!!!!!!!!!!! Avoiding an infinite loop !!!!!!!!!!!!\n")
            return 1, p1
        else:
            rounds.add(tmp)

        # Draw cards and update deck for each player
        c1, c2 = p1[0], p2[0]
        p1, p2 = p1[1:], p2[1:]
        
        if len(p1) >= c1 and len(p2) >= c2:
            # recurse!
            winner, _ = recursiveCombat(p1[:c1], p2[:c2], subgame=subgames+1)
            if winner == 1:
                p1.append(c1)
                p1.append(c2)
            elif winner == 2:
                p2.append(c2)
                p2.append(c1)
            else:
                raise ValueError("Unexpected winner", winner)
        else:
            # normal round
            if c1 > c2:
                p1.append(c1)
                p1.append(c2)
            else:
                p2.append(c2)
                p2.append(c1)

        # check for potential winner
        if len(p1) == 0:
            return 2, p2
        elif len(p2) == 0:
            return 1, p1

# First part
original_p1, original_p2 = readInput()
p1, p2 = list(original_p1), list(original_p2)
while p1 != [] and p2 != []:
    # Draw cards and update deck for each player
    c1, c2 = p1[0], p2[0]
    p1, p2 = p1[1:], p2[1:]

    if c1 > c2:
        p1.append(c1)
        p1.append(c2)
    else:
        p2.append(c2)
        p2.append(c1)

if len(p1) > 0: print(computeScore(p1))
else: print(computeScore(p2))

# Second part
winner, deck = recursiveCombat(list(original_p1), list(original_p2))
print(winner, computeScore(deck))
