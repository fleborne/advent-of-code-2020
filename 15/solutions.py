import numpy as np

target = 30000000
#target = 2020

inputs = [20,0,1,11,6,3]
#inputs = [1,3,2]
#inputs = [0, 3, 6]
#inputs = [2,1,3]

spoken_numbers = dict() # (last_index_spoken, last_last_index_spoken, number_of_times_spoken)
numbers = np.zeros((target))

def processNumber(i, numbers):
    n = numbers[i]
    try:
        ls, lls, ns = spoken_numbers[n]
        spoken_numbers[n][0] = i
        spoken_numbers[n][1] = ls
        spoken_numbers[n][2] += 1
    except KeyError:
        spoken_numbers[n] = [i, -1, 1]

for i in range(len(inputs)):
    numbers[i] = inputs[i]
    processNumber(i, numbers)

for i in range(len(inputs), target):
    print(i/target*100.0, end="\r")
    try:
        pass
        #print(spoken_numbers)
        #print(numbers[:i])
        #_ = input("")
    except SyntaxError:
        pass

    try:
        last_i, llast_i, nb_spoken = spoken_numbers[numbers[i-1]]
        if nb_spoken == 1 and llast_i == -1:
            numbers[i] = 0
        else:
            numbers[i] = last_i - llast_i
    except KeyError:
        numbers[i] = 0

    processNumber(i, numbers)

print(numbers[2019])
print(numbers[-1])
