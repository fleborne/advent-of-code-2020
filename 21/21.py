import sys

def readInput():
    return open(sys.argv[1], "r").read().strip().splitlines()

allergens = dict() # allergens[allergen] = ingredients possibly containing it
ingredients = set()
ingredients_appearances = []

for line in readInput():
    ingreds, allergs = line[:-1].split(' (contains ')
    ingreds = ingreds.split(' ')
    allergs = allergs.split(', ')
    
    ingredients = set.union(ingredients, (set(ingreds)))

    for i in ingreds:
        ingredients_appearances.append(i)

    for a in allergs:
        if a not in allergens.keys():
            allergens[a] = set(ingreds)
        else:
            allergens[a] = allergens[a].intersection(set(ingreds))

safe_ingredients = set(ingredients)
for key, val in allergens.items():
    safe_ingredients -= val
print(sum([ingredients_appearances.count(si) for si in safe_ingredients]))

stop = False
while not stop:
    stop = True
    for key, val in allergens.items():
        if len(val) == 1:
            val = list(val)[0]
            for k in allergens.keys():
                if k != key and val in allergens[k]:
                    stop = False
                    allergens[k].remove(val)

result = sorted([(key, list(val)[0]) for key,val in allergens.items()], key=lambda elt: elt[0])
print(','.join([r[1] for r in result]))
