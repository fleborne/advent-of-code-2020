import sys

def isPassportValid(passport):
    required_fields = ["byr",
                       "iyr",
                       "eyr",
                       "hgt",
                       "hcl",
                       "ecl",
                       "pid"]

    is_valid = True

    for rf in required_fields:
        is_valid &= (rf in passport.keys())

    return is_valid

passports = []
tmp_passport = dict()
with open(sys.argv[1], "r") as f:
    for line in f:
        if line == '\n':
            passports.append(tmp_passport)
            tmp_passport = dict()
        else:
            line = line.strip()
            fields = line.split(' ')
            for field in fields:
                key, value = field.split(':')
                tmp_passport[key] = value

    passports.append(tmp_passport)

## First star
valid_passports_count = 0
for p in passports:
    if isPassportValid(p):
        valid_passports_count += 1

print("The answer for the first star is {}".format(valid_passports_count))

## Second star
def checkYear(value, min, max):
    try:
        value = int(value)
        return (value > min-1) and \
               (value < max+1)
    except:
        return False

def checkValidityOfField(key, value):
    # byr (Birth Year) - four digits; at least 1920 and at most 2002.
    if key == "byr":
        return checkYear(value, 1920, 2002)

    # iyr (Issue Year) - four digits; at least 2010 and at most 2020.
    if key == "iyr":
        return checkYear(value, 2010, 2020)

    # eyr (Expiration Year) - four digits; at least 2020 and at most 2030.
    if key == "eyr":
        return checkYear(value, 2020, 2030)

    # hgt (Height) - a number followed by either cm or in:
    #     If cm, the number must be at least 150 and at most 193.
    #     If in, the number must be at least 59 and at most 76.
    if key == "hgt":
        unit = value[-2:]

        try:
            quantity = int(value[:-2])
        except:
            return False

        if unit == "cm":
            return (149 < quantity) and (quantity < 194)
        elif unit == "in":
            return (58 < quantity) and (quantity < 77)
        else:
            return False

    # hcl (Hair Color) - a # followed by exactly six characters 0-9 or a-f.
    if key == "hcl":
        if (len(value) == 7) and (value[0] == '#'):
            for c in value[1:]:
                if not ((('a' <= c) and (c <= 'f')) or \
                        (('0' <= c) and (c <= '9'))):
                    return False
            return True
        else:
            return False

    # ecl (Eye Color) - exactly one of: amb blu brn gry grn hzl oth.
    if key == "ecl":
        return value in ["amb", "blu", "brn", "gry", "grn", "hzl", "oth"]

    # pid (Passport ID) - a nine-digit number, including leading zeroes.
    if key == "pid":
        if len(value) == 9:
            try:
                value = int(value)
                return True
            except:
                return False
        else:
            return False

    # cid (Country ID) - ignored, missing or not.
    if key == "cid":
        return True

def isPassportValid(passport):
    required_fields = ["byr",
                       "iyr",
                       "eyr",
                       "hgt",
                       "hcl",
                       "ecl",
                       "pid"]

    is_valid = True

    for rf in required_fields:
        if rf in passport.keys():
            is_valid &= checkValidityOfField(rf, passport[rf])
        else:
            return False

    return is_valid

valid_passports_count = 0
for p in passports:
    if isPassportValid(p):
        valid_passports_count += 1
print("The answer for the second star is {}".format(valid_passports_count))
