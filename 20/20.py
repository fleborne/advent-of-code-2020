import sys
import math

tile_size = 10

def readInput():
    inputs = open(sys.argv[1], "r").read().strip().split('\n\n')

    tiles = dict()
    for tile in inputs:
        tile = tile.splitlines()
        tileid = int(tile[0].replace(':', '').split()[1])
    
        tilecontent = [[None for j in range(tile_size)] for i in range(tile_size)]
        for r, line in enumerate(tile[1:]):
            for c, char in enumerate(line):
                tilecontent[r][c] = char 
        tiles[tileid] = tilecontent

    return tiles

def printTile(tile, id_=None):
    print("Tile", id_)
    for line in tile:
        print(''.join(line))

def printArt(art):
    for i in range(len(art)):
        for j in range(tile_size):
            line = [''.join(tile[tile_size - 1 - j]) for tile in art[i]]
            print(''.join(line))

def cleanArt(art):
    cleaned_art = []
    for i in range(len(art)):
        for j in range(1, tile_size-1):
            line = [''.join(tile[tile_size - 1 - j][1:-1]) for tile in art[i]]
            cleaned_art.append(''.join(line))
            print(''.join(line))
    return cleaned_art

def flipXTile(tile):
    new_tile = [[] for i in range(len(tile))]
    for r, line in enumerate(tile):
        new_tile[r] = list(reversed(line))

    return new_tile

def rotateOnceTile(tile):
    new_tile = [[None for j in range(len(tile))] for i in range(len(tile))]
    for r, line in enumerate(tile):
        for c, char in enumerate(line):
            new_tile[len(tile)-c-1][r] = char

    return new_tile

def rotateTwiceTile(tile):
    return rotateOnceTile(rotateOnceTile(tile))

def rotate3xTile(tile):
    return rotateTwiceTile(rotateOnceTile(tile))

def flipYTile(tile):
    return list(reversed(tile))

def isMatchingTop(tile0, tile1):
    return ''.join(tile0[0]) == ''.join(tile1[-1])

def isMatchingBottom(tile0, tile1):
    return isMatchingTop(tile1, tile0)

def isMatchingRight(tile0, tile1):
    edge0 = [line[-1] for line in tile0]
    edge1 = [line[0] for line in tile1]
    return ''.join(edge0) == ''.join(edge1)

def isMatchingLeft(tile0, tile1):
    return isMatchingRight(tile1, tile0)

def flips(tile):
    yield tile
    yield flipXTile(tile)
    yield flipYTile(tile)

def rotations(tile):
    yield tile
    yield rotateOnceTile(tile)
    yield rotateTwiceTile(tile)
    yield rotate3xTile(tile)

def transforms(tile):
    tfnum = 0
    for rot in rotations(tile):
        for flip in flips(rot):
           yield tfnum, flip
           tfnum += 1
     
def getMatchingTile(tile0, tile1):
    for tfid, tiletf in transforms(tile1):
        if isMatchingTop(tile0, tiletf) or \
           isMatchingRight(tile0, tiletf) or \
           isMatchingBottom(tile0, tiletf) or \
           isMatchingLeft(tile0, tiletf):
            return tiletf

def findMatches(tiles, tileid):
    tile0 = tiles[tileid]
    matches = dict(top=[], right=[], bottom=[], left=[])
    
    for tile1id, tile1 in tiles.items():
        if tile1id != tileid:
            for tfid, tiletf in transforms(tile1):
                if isMatchingTop(tile0, tiletf):
                    matches["top"].append((tile1id, tfid))

                elif isMatchingRight(tile0, tiletf):
                    matches["right"].append((tile1id, tfid))

                elif isMatchingBottom(tile0, tiletf):
                    matches["bottom"].append((tile1id, tfid))

                elif isMatchingLeft(tile0, tiletf):
                    matches["left"].append((tile1id, tfid))

    return matches

def matchMonster(art, rastart, castart, monster):
    for rm, linem in enumerate(monster):
        for cm, charm in enumerate(linem):
            if charm == '#' and art[rastart+rm][castart+cm] != '#':
                return False
    return True

def searchMonster(art, monster):
    monsters = 0
    rough_waters = 0
    for r in range(len(art)):
        for c in range(len(art[r])):
            if art[r][c] == '#':
                rough_waters += 1
            try:
                if matchMonster(art, r, c, monster):
                    monsters += 1
            except IndexError:
                pass

    return monsters, rough_waters


tiles = readInput()
img_shape = (int(math.sqrt(len(tiles))),) * 2

corners = []
sides = []
cornersidprod = 1

links = dict()

for key, value in tiles.items():
    matches = findMatches(tiles, key)
    if matches["top"] == [] or matches["bottom"] == []:
        if matches["left"] == [] or matches["right"] == []:
            corners.append(key)
            cornersidprod *= key
    if matches["top"] == [] or \
            matches["right"] == [] or \
            matches["bottom"] == [] or \
            matches["left"] == []:
        sides.append(key)

    if len(matches["top"]) > 2:
        print("warntop")
    if len(matches["bottom"]) > 2:
        print("warnbottom")
    if len(matches["left"]) > 2:
        print("warnleft")
    if len(matches["right"]) > 2:
        print("warnright")

    links[key] = []
    for side in ["top", "right", "bottom", "left"]:
        if matches[side] != []:
            links[key].append(matches[side][0][0])

print("cornersidprod", cornersidprod)
#print("links", '\n'.join([str(key)+': '+','.join(map(str, val)) for key, val in sorted(links.items(), key=lambda elt: elt[0])]))

img = [[None for j in range(img_shape[1])] for i in range(img_shape[0])]
img[0][0] = corners[0]
img[0][1] = links[corners[0]][0]
img[1][0] = links[corners[0]][1]

art = [[[[' '*tile_size] for k in range(tile_size)] for j in range(img_shape[1])] for i in range(img_shape[0])]
art[0][0] = tiles[img[0][0]]
art[1][0] = getMatchingTile(art[0][0], tiles[img[0][1]])
art[0][1] = getMatchingTile(art[0][0], tiles[img[1][0]])

taken = [corners[0], *links[corners[0]]]

for r in range(img_shape[0]):
    for c in range(img_shape[1]):
        #print('\n')
        #print(r, c)
        #for line in img:
        #    print(' '.join(map(str, line)))
        if r == 0 and c == 0:
            continue
        elif r == 0 and c < img_shape[1]-1:
            for candidate in links[img[r][c]]:
                if candidate in sides and candidate not in taken:
                    img[r][c+1] = candidate
                    art[c+1][r] = getMatchingTile(art[c][r], tiles[img[r][c+1]])
                    taken.append(candidate)
        elif c == 0 and r < img_shape[0]-1:
            for candidate in links[img[r][c]]:
                if candidate in sides and candidate not in taken:
                    img[r+1][c] = candidate
                    art[c][r+1] = getMatchingTile(art[c][r], tiles[img[r+1][c]])
                    taken.append(candidate)
        
        if c < img_shape[1]-1 and 0 < r:
            for candidate in links[img[r][c]]:
                if candidate in links[img[r-1][c+1]] and candidate not in taken:
                    img[r][c+1] = candidate
                    art[c+1][r] = getMatchingTile(art[c][r], tiles[img[r][c+1]])
                    taken.append(candidate)
monster = [None]*3
monster[0] = "                  # "
monster[1] = "#    ##    ##    ###"
monster[2] = " #  #  #  #  #  #   "

cleaned_art = cleanArt(art)

for _, arttf in transforms(cleaned_art):
    monsters, rough_waters = searchMonster(arttf, monster)
    if monsters > 0:
        break
    
print(rough_waters - monsters * sum([line.count('#') for line in monster]))
