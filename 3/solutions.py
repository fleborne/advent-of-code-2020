import sys
import numpy as np

num_lines = 323
num_cols = 31
inputs = np.zeros((num_lines, num_cols), dtype=np.int)

with open(sys.argv[1], "r") as f:
    for line_idx, line in enumerate(f):
        for col_idx, c in enumerate(line):
            if c == '#':
                inputs[line_idx, col_idx] = 1

def findNumOfEncounteredTrees(dp, start_position=(0, 0)):
    position = list(start_position)
    trees_encountered = 0

    while position[0] < num_lines:
        if inputs[position[0], position[1] % num_cols] == 1:
            trees_encountered += 1

        position[0] += dp[0]
        position[1] += dp[1]

    return trees_encountered

## First star
print("The answer for the first star is {}".format(findNumOfEncounteredTrees((1, 3))))

## Second star
trees_by_slope = []
trees_by_slope.append(findNumOfEncounteredTrees((1, 1)))
trees_by_slope.append(findNumOfEncounteredTrees((1, 3)))
trees_by_slope.append(findNumOfEncounteredTrees((1, 5)))
trees_by_slope.append(findNumOfEncounteredTrees((1, 7)))
trees_by_slope.append(findNumOfEncounteredTrees((2, 1)))

product_of_trees = 1
for trees in trees_by_slope:
    product_of_trees *= trees

print("The answer for the second star is {}".format(product_of_trees))
