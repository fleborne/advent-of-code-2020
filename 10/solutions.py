import sys, math
import functools

def readInput(filename=None):
    if filename is None: filename = sys.argv[1]
    return open(filename, "r").read().strip().split('\n')

joltages = [0] + sorted(list(map(int, readInput())))
print(joltages)
j_diffs = [elt-joltages[i] for i, elt in enumerate(joltages[1:])]
j_diffs.append(3)
print(j_diffs.count(3)*j_diffs.count(1))

@functools.lru_cache(maxsize=1024)
def countWays(idx):
    if idx == len(joltages) - 1:
        return 1

    possibilities = sum(map(lambda x: 1 if x <= joltages[idx]+3 else 0, joltages[idx+1:min(idx+4, len(joltages))]))

    if possibilities == 1:
        result = countWays(idx+1)
    else:
        result = sum([countWays(idx+1+i) for i in range(possibilities)])

    return result

print(countWays(0))
