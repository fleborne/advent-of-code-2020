def transformSubjectNumber(subject_number, loop_size):
    #return (subject_number ** loop_size) % 20201227

    value = 1
    for i in range(loop_size):
        value = (value * subject_number) % 20201227

    return value

def findLoopSize(pk):
    sn = 7
    m = 20201227
    ls = 1

    tmp = 1
    loop_size = 0

    while tmp != pk:
        tmp = (tmp * sn) % m
        loop_size += 1

    return loop_size

card_pub_key = 14222596
door_pub_key = 4057428

card_private_key = findLoopSize(card_pub_key)
print("Card PK", card_private_key)
door_private_key = findLoopSize(door_pub_key)
print("Door PK", door_private_key)

print("Encryption Key", transformSubjectNumber(door_pub_key, card_private_key))
