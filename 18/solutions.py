import sys
import operator as ope

def readInputs():
    return open(sys.argv[1], "r").read().strip().splitlines()

def findMatchingParenthesis(expr):
    #print("to match", expr)
    opening = 1
    closing = 0

    for i, c in enumerate(expr[1:]):
        if c == '(':
            opening += 1
        elif c == ')':
            closing += 1

        if opening == closing:
            return i+1

def parseExpression(expr, tmp_result):
    expr = expr.replace(' ', '')
    #print("to parse", expr, "tmp", tmp_result)

    if tmp_result == '(':
        j = findMatchingParenthesis(expr)
        tmp_result = parseExpression(expr[1:j], expr[1])
        i = j + 1
    else:
        tmp_result = int(tmp_result)
        i = 1

    op = None

    while i < len(expr):
        c = expr[i]
        #print("parsing", c, "at", i, "in", expr, "tmp", tmp_result)
        if c == '+': op = ope.add
        elif c == '*': op = ope.mul
        elif c == '(':
            j = findMatchingParenthesis(expr[i:])
            arg2 = parseExpression(expr[i+1:i+j], expr[i+1])
            #print(op, tmp_result, arg2)
            tmp_result = op(tmp_result, arg2)
            i += j + 1 
            continue
        else:
            #print(op, tmp_result, int(c))
            tmp_result = op(tmp_result, int(c))
            op = None

        i += 1

    return tmp_result

inputs = readInputs()
print(sum([parseExpression(expr, expr[0]) for expr in inputs]))

def infixToPostfix(expr):
    opstack = []
    output = []

    i = 0
    while i < len(expr):
        c = expr[i]
        #print(opstack, output)
        if c not in ['(', ')', '+', '*']:
            output.append(c)
        else:
            if opstack == [] or opstack[-1] == '(':
                opstack.append(c)
            elif c == '(':
                opstack.append(c)
            elif c == ')':
                p = opstack.pop()
                while p != '(':
                    output.append(p)
                    p = opstack.pop()
            elif c == '+' and opstack[-1] == '*':
                opstack.append(c)
            elif c == opstack[-1]:
                opstack.append(c)
            else:
                p = opstack.pop()
                output.append(p)
                i -= 1
        i += 1

    for op in opstack[::-1]:
        output.append(op)

    return ''.join(output)

def computePostfix(expr):
    s = []
    for c in expr:
        try:
            s.append(int(c))
        except ValueError:
            v1 = s.pop()
            v2 = s.pop()
            
            if c == '+':
                op = ope.add
            elif c == '*':
                op = ope.mul
            else:
                raise ValueError()
            s.append(op(v1, v2))

    return s.pop()

#print(computePostfix(infixToPostfix(sys.argv[1].replace(' ', ''))))
#inputs = readInputs()
print(sum(map(computePostfix, map(infixToPostfix, map(lambda x: x.replace(' ', ''), inputs)))))
