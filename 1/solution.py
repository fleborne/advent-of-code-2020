## First star

inputs = []

with open("input.txt", "r") as f:
    for line in f:
        inputs.append(int(line))

for idx_a, a in enumerate(inputs):
    for idx_b in range(len(inputs)-1, idx_a, -1):
        b = inputs[idx_b]

        if a+b == 2020:
            print("The answer is {}".format(a*b))


## Second star
for idx_a, a in enumerate(inputs):
    for idx_b in range(len(inputs)-1, idx_a, -1):
        for idx_c in range(idx_a+1, idx_b):
            b = inputs[idx_b]
            c = inputs[idx_c]

            if a+b+c == 2020:
                print("The answer is {}".format(a*b*c))
