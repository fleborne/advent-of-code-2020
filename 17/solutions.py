import sys
import copy

def readInputs():
    return open(sys.argv[1], "r").read().strip().splitlines()

inputs = readInputs()

size_y = len(inputs)
size_x = len(inputs[0])

# grid[w][z][y][x] = state of cell (x, y, z, w)
grid = dict()
grid[0] = dict()
grid[0][0] = dict()

for i, line in enumerate(inputs):
    grid[0][0][i-(size_y-1)//2] = dict()
    for j, c in enumerate(line):
        grid[0][0][i-(size_y-1)//2][j-(size_x-1)//2] = c

def countActiveNeighbors(grid, x, y, z, w):
    active_n = 0
    for w_n in [w-1, w, w+1]:
        for z_n in [z-1, z, z+1]:
            for y_n in [y-1, y, y+1]:
                for x_n in [x-1, x, x+1]:
                    if x == x_n and y == y_n and z == z_n and w == w_n:
                        continue
                    else:
                        try:
                            if grid[w_n][z_n][y_n][x_n] == '#':
                                active_n +=1
                        except KeyError:
                            pass
    return active_n

def updateCell(grid, x, y, z, w):
    active_n = countActiveNeighbors(grid, x, y, z, w)
    try:
        old_state = grid[w][z][y][x]
    except KeyError:
        old_state = '.'

    if old_state == '#':
        if active_n not in [2, 3]:
            return '.'
        else:
            return '#'
    else:
        if active_n == 3:
            return '#'
        else:
            return '.'

def updateGrid(grid):
    new_grid = dict()

    max_w = max(grid.keys())
    max_z = max(grid[0].keys())
    max_y = max(grid[0][0].keys())
    max_x = max(grid[0][0][0].keys())

    for w in range(-max_w-1, max_w+2):
        new_grid[w] = dict()
        for z in range(-max_z-1, max_z+2):
            new_grid[w][z] = dict()
            for y in range(-max_y-1, max_y+2):
                new_grid[w][z][y] = dict()
                for x in range(-max_x-1, max_x+2):
                    new_grid[w][z][y][x] = updateCell(grid, x, y, z, w)
    
    return new_grid

def countActive(grid):
    actives = 0
    for w in grid.keys():
        for z in grid[w].keys():
            for y in grid[w][z].keys():
                for x in grid[w][z][y].keys():
                    if grid[w][z][y][x] == '#':
                        actives += 1
    return actives

def printGrid(grid):
    for w in sorted(grid.keys()):
        for z in sorted(grid[w].keys()):
            print("z", z, "w", w)
            for y in sorted(grid[w][z].keys()):
                print("".join(grid[w][z][y].values()))

for i in range(6):
#    printGrid(grid)
    grid = updateGrid(grid)

print(countActive(grid))

